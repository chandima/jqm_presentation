<?php
header('Content-Type: application/json');
    
$feedid = htmlspecialchars($_GET["id"]);
$url = 'http://www.npr.org/rss/rss.php?id='.$feedid;
$xml = simplexml_load_file( $url );
$json = json_encode( $xml );
print_r($json);