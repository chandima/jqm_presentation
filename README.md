## jQuery Mobile: Building mobile websites and applications

![Mou icon](http://jquerymobile.com/demos/1.2.0/docs/_assets/images/jquery-logo.png)

## Overview

Initially compiled for the *ASU Dev Day* presentation.

1. All source files
2. Presentation (PDF)
3. Zipped Xcode project (Xcode 4.5.1 & Cordova 2.1.0)
